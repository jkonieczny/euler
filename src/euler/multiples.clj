(ns euler.multiples
    ;(:import )
    ;(:require )
)

(def maks 1000)
(defn euler
  "Testujemy klo?er"
  [current sum]
  (let 
    [suma (if 
            (or  (= 0 (rem current 3))  (= 0 (rem current 5)) )
           ; (+ sum current)
            (+ sum current) 
            sum
           )]
   (if (< current (dec maks))
     (recur (inc current) suma)
     suma
     )
    )
)
(println (euler 0 0))