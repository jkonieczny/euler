(ns euler.max_palindrome)

(def max1 999)
(def max2 999)

(defn is-palindrome 
	[number_string]
	(let [
		  len (.length number_string)
	]
	(if (or (= len 1) (= len 0))
		true
		(if (= (get number_string 0) (get number_string (- len 1)))
		 (recur (subs number_string 1 (- len 1)))
		 false
		)
	))
)


(defn max-pal 
	"here's the magic"
	[num1 num2 maxpal]
	(if (or (>= num1 100) (>= num2 100))
		(let [cur  (* num1 num2)]
			(if (and (is-palindrome (str cur)) (> cur maxpal))
				(recur num1 num2 cur)
				(if (>= num1 100)
					(recur (dec num1) num2 maxpal)
					(recur max1 (dec num2) maxpal)
				)
			)

		)
		maxpal
	)
)


;(if (is-palindrome "110011") (println "TRUE") (println "FALSE") )
(time (println "Max palindrome number: " (max-pal max1 max2 0)))
