(ns euler.even_fibonacci
    ;(:import )
    ;(:require )
)

(def maxval 4000000)

(defn fibo 
    "fibonacci with even elements"
    [prev curr sum]
    (let 
      [sum (if (= 0 (rem curr 2)) (+ sum curr) sum )
       next (+ prev curr)
       ]
      (if (< next maxval)  (recur curr next sum) sum ) ) )

(println (fibo 1 2 0))